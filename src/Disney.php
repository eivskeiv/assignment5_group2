<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();


        $actorElements = $this->xpath->query('Actors')->item(0);
        $actors = $actorElements->childNodes;


        foreach ($actors as $actorName) {

            if ($actorName->nodeType == XML_ELEMENT_NODE) {
                // To store movie information
                $movieArray = array();
                // The actors ID
                $actorId = $actorName->getAttribute('id');
                // Returns every role the actor has played
                $movieRoles = $this->xpath->query('Subsidiaries/Subsidiary/Movie/Cast/Role[@actor="'. $actorId .'"]');


                // Goes through every role and finds which movie information about the movie
                foreach ($movieRoles as $movie) {
                  if ($movie->nodeType == XML_ELEMENT_NODE) {

                    //Finds movie year, movie name, the actors role and concatenates everything together to one string
                    $movieYear = $movie->parentNode->parentNode->getElementsByTagName("Year")->item(0)->textContent;
                    $movieName = $movie->parentNode->parentNode->getElementsByTagName("Name")->item(0)->textContent;
                    $actorRole = $movie->getAttribute("name");
                    $actorPlay = 'As '.$actorRole.' in '.$movieName.' ('.$movieYear.')';

                    //Stores string in array
                    $movieArray[] = $actorPlay;

                  }
                }
                // Finds the actors name and stores it in a array
                $actorNm = $actorName->getElementsByTagName("Name")->item(0)->textContent;
                $result[$actorNm] = $actorNm;
                // Stores the movie array under the actors name
                $result[$actorNm] = $movieArray;


            }
        }

        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors() {

        $deleteArr = array();

        $actorElements = $this->xpath->query('Actors')->item(0);
        $actors = $actorElements->childNodes;


        foreach ($actors as $actor) {
              // Is not text node
            if ($actor->nodeType == XML_ELEMENT_NODE) {

                // The actors ID
                $actorId = $actor->getAttribute('id');

                // Returns a list of movies the actor has acted in
                $movieList = $this->xpath->evaluate('count(Subsidiaries/Subsidiary/Movie/Cast/Role[@actor="'. $actorId .'"])');

                // if movieList is empty
                if ($movieList == 0) {
                    //stores the actor node
                    $deleteArr[] = $actor;
                }
            }
        }

        // deletes the actors
        foreach ($deleteArr as $key) {
          $actorElements->removeChild($key);
        }

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null) {
        //To do:
        // Implement functionality as specified

        $movie = $this->xpath->query('Subsidiaries/Subsidiary[@id ="'.$subsidiaryId.'"]/Movie[Name="'.$movieName.'" and Year="'.$movieYear.'"]')->item(0);
        $cast = $movie->getElementsByTagName("Cast")->item(0);

        $newRole = $this->doc->createElement('Role');
        $cast->appendChild($newRole);
        $newRole->setAttribute("name", $roleName);
        $newRole->setAttribute("actor", $roleActor);
        $newRole->setAttribute("alias", $roleAlias);


    }
}
?>
